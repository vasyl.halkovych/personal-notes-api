const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

const auth = require('./middleware/auth');

const route_auth = require('./routes/auth');
const route_user = require('./routes/user');
const route_note = require('./routes/note');

const app = express();

app.use(morgan('tiny'));

app.use(express.json({ extended: true }));

app.use('/api/auth', route_auth);
app.use(auth);
app.use(route_user);
app.use('/api/notes', route_note);

app.use((req, res) => {
	res.status(404).send({ message: 'Page not found!' });
});

app.use((err, req, res, next) => {
	res.status(500).send({ message: 'Server error' });
});

const start = async () => {
	try {
		await mongoose.connect('mongodb+srv://testuser1:testuser1@personalnotes.qukvh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
			{ useNewUrlParser: true, useUnifiedTopology: true });
		console.log('Connected')
		app.listen(process.env.PORT || 8080);
        console.log('Server is running on port 8080');
	} catch (error) {
		res.status(500).send({ message: error.message });
	}
};

start();
