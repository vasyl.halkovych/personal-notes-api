const express = require('express');
const router = express.Router();

const note = require('../controllers/note');

router.get('/:id', note.getNoteByIdForUser);
router.put('/:id', note.updateNoteByIdForUser);
router.patch('/:id', note.checkUnchekNoteByIdForUser);
router.delete('/:id', note.deleteNoteByIdForUser);
router.post('/', note.postNoteForUser);
router.get('/', note.getNotesForUser);


module.exports = router;
