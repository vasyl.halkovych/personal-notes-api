const express = require('express');
const router = express.Router();

const user = require('../controllers/user');


router.get('/api/users/me', user.getProfile);
router.delete('/api/users/me', user.deleteProfile);
router.patch('/api/users/me', user.patchProfile);

module.exports = router;
