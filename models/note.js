const mongoose = require('mongoose');
const Note = mongoose.model('Note', {
	userId: {
		type: mongoose.Schema.Types.ObjectId
	},
	completed: Boolean,
	text: String,
	createdDate: {
		type: Date,
		default: Date.now()
	}
});

module.exports = Note;
