const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

exports.postRegister = async (req, res) => {
	try {
		const { username, password } = req.body;

		const existingUser = await User.findOne({ username });
		if (existingUser) {
			return res.status(400)
				.send({ message: 'User already exists. Try login' });
		}

		const user = new User({
			username,
			password: await bcrypt.hash(password, 10)
		});
		await user.save();

		res.status(200).send({ message: 'Success' });
	} catch (err) {
		res.status(500).send({ message: err.message });
	}
};

exports.postLogin = async (req, res) => {
	try {
		const { username, password } = req.body;
		const user = await User.findOne({ username });

		if (!user) {
			res.send({ message: 'Invalid username or password!' });
		}

		if (!(await bcrypt.compare(password, user.password))) {
			res.send({ message: 'Invalid username or password!' });
		}

		const jwt_token = jwt.sign({
			_id: user._id,
			username: user.username,
			createdDate: user.createdDate
		}, 'secretKey');

		res.status(200).send({ message: 'Logged in successfully', jwt_token });
	} catch (err) {
		res.status(500).send({ message: 'Server error!' });
	}
};
