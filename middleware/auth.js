const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {
	try {
		const { authorization } = req.headers;
		const token = authorization.split(' ')[1];
		console.log(authorization)

		if (!authorization) {
			return res.status(400)
				.send({ message: 'Please, provide "Authorization" header!' });
		}

		if (!token) {
			return res.status(400)
				.send({ message: 'Please, include "token" to request' });
		}

		const payload = jwt.verify(token, 'secretKey');
		req.user = {
			userId: payload._id,
			username: payload.username
		};
		next();
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

module.exports = auth;
